
const timeout_getTokeninfo = 60;
const timeoutCheck_getTokeninfo = (inputs) => new Promise((resolve, reject) => {
    // use timeout, if abc don't run, we'll skip abc function, but not sure abc still run in background or not
    const timeoutInstance = setTimeout(() => {
        reject('timeout');
    }, timeout_getTokeninfo * 1000); // 60 sec
    //read token information from contract
    async function getTokeninfo(w3, token_lp, token_a, token_base) {
        async function gettotalSupply(w3, tokenaddr) {
            const erc20Abi = [{ "constant": true, "inputs": [], "name": "name", "outputs": [{ "name": "", "type": "string" }], "payable": false, "type": "function" },
            { "constant": true, "inputs": [], "name": "totalSupply", "outputs": [{ "name": "", "type": "uint256" }], "payable": false, "type": "function" },
            { "constant": true, "inputs": [], "name": "decimals", "outputs": [{ "name": "", "type": "uint8" }], "payable": false, "type": "function" },
            { "constant": true, "inputs": [{ "name": "_owner", "type": "address" }], "name": "balanceOf", "outputs": [{ "name": "balance", "type": "uint256" }], "payable": false, "type": "function" }];
            const contract = new w3.eth.Contract(erc20Abi, tokenaddr);
            const result = await contract.methods.totalSupply().call();
            const format = w3.utils.fromWei(result);
            return format;
        }

        async function getBalance(w3, waddr, tokenaddr) {
            const minABI = [
                {
                    constant: true,
                    inputs: [{ name: "_owner", type: "address" }],
                    name: "balanceOf",
                    outputs: [{ name: "balance", type: "uint256" }],
                    type: "function",
                },
                // decimals
                //https://piyopiyo.medium.com/how-to-get-erc20-token-balance-with-web3-js-206df52f2561
                {
                    constant: true,
                    inputs: [],
                    name: "decimals",
                    outputs: [{ name: "", type: "uint8" }],
                    type: "function",
                },
            ];
            const contract = new w3.eth.Contract(minABI, tokenaddr);
            const Decimals = await contract.methods.decimals().call();
            const result = await contract.methods.balanceOf(waddr).call();
            const format = result / (10 ** Decimals);
            return format;
        }

        if (debug_level >= 5)
            console.log('[debug][5]:', 'before getBalance(w3, token_lp, token_a)');
        const holding_a = await getBalance(w3, token_lp, token_a);
        const holding_base = await getBalance(w3, token_lp, token_base);
        const totalSupply = await gettotalSupply(w3, token_lp);
        if (debug_level >= 5)
            console.log('[debug][5]:', 'after gettotalSupply(w3, token_lp)');
        return { totalSupply, holding_a, holding_base };
    };

    (async () => {
        const web3 = new Web3(position['lp_contract']['rpc']);
        const { totalSupply, holding_a, holding_base } = await getTokeninfo(web3, position['token_lp']['addr'], position['token_a']['addr'], position['token_base']['addr']);
        lp_supply = totalSupply;
        lp_tokenA = holding_a;
        lp_tokenBase = holding_base;
        clearTimeout(timeoutInstance);
        resolve('success');
    })();

});


try {
    const lp_info = await timeoutCheck_getTokeninfo();
    if (lp_info === 'timeout') {
        throw ('timeoutCheck_getTokeninfo ' + timeout_getTokeninfo);
    }
} catch (e) {
    console.log('[ERROR] ' + Date().toString());
    throw (e);
}
