const portfolio_binance = async () => {
    const Mutex = require('async-mutex').Mutex;
    const Binance = require('node-binance-api');

    const binance = new Binance().options({
        APIKEY: 'APIKEY',
        APISECRET: 'APISECRET'
      });

    let mutex = new Mutex();
    let usdValue_account_overview = 0;
    const release = await mutex.acquire();
    binance.balance((error, balances) => {
        if (error) return console.error(error);
        usdValue_account_overview += 123;
        console.info('binance return: $',usdValue_account_overview);
        release();
    });

    // 要等 binance.balance 的結果
    const release2 = await mutex.acquire();
    release2();
    return usdValue_account_overview;
}

(async () => {
    const binance_overview = await portfolio_binance();
    console.log('binance_overview: $', binance_overview);
})();